const p20 = document.querySelector('.p-20');
const p80 = document.querySelector('.p-80');

p20.addEventListener("click", e => {
    let tg = e.target;
    if (tg.classList.contains('card')) {
        ajax('https://jsonplaceholder.typicode.com/posts', showPost, tg.dataset.id)
        console.log(tg)
    }else if(tg.parentElement.classList.contains('card')){
        tg = tg.parentElement
        ajax('https://jsonplaceholder.typicode.com/posts', showPost, tg.dataset.id)
        console.log(tg)
    }
});


ajax('https://jsonplaceholder.typicode.com/users', showUsers)

function ajax(url, targetFunc, dataSet) {
    const xhr = new XMLHttpRequest();
    xhr.onload = () => {
        targetFunc(xhr.responseText, dataSet)
    }
    xhr.open('GET', url);
    xhr.send();
}

function showUsers(users) {
    users = JSON.parse(users);

    users.forEach(item => {
        p20.innerHTML += `<div class="card" data-id="${item.id}">
                                <p>${item.name}</p>
                                <p>${item.username}</p>
                                <p>${item.address.city} - ${item.address.street}</p>
                            </div>`
    })
}

function showPost(posts, dataSet) {
    posts = JSON.parse(posts);

    p80.innerHTML = '';
    posts.forEach(item => {
        if(item.userId == dataSet){
            p80.innerHTML += `<div class="card">
                                <h3>${item.title}</h3>
                                <p>${item.body}</p>
                            </div>`
        }
        
    })
}